<?php

use yii\db\Migration;

/**
 * Class m220921_193846_add_columns_category_table
 */
class m220921_193846_add_columns_category_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
      $this->addColumn('{{%category}}', 'blocks_order', $this->tinyInteger(1));
      $this->addColumn('{{%category}}', 'hidden', $this->tinyInteger(1));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
        $this->dropColumn('{{%category}}', 'blocks_order');
        $this->dropColumn('{{%category}}', 'hidden');
	}

}
