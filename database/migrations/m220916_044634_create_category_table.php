<?php

use yii\db\Migration;
use app\traits\migrations\CreateTableOptions;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m220916_044634_create_category_table extends Migration
{
	use CreateTableOptions;

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		$this->createTable('{{%category}}', [
			'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
			'h1' => $this->string(255),
			'slug' => $this->string(255),
			'parent_id' => $this->integer()->defaultValue(0),
			'descr_1' => $this->text(),
			'descr_2' => $this->text(),
			'image' => $this->string(255),
            'order_id' => $this->integer(),
            'superjob' => $this->text(),
            'show_questionnaire' => $this->integer(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'root' => $this->integer(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'lvl' => $this->smallInteger(5)->notNull(),
            'icon' => $this->string(255),
            'icon_type' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'selected' => $this->boolean()->notNull()->defaultValue(false),
            'disabled' => $this->boolean()->notNull()->defaultValue(false),
            'readonly' => $this->boolean()->notNull()->defaultValue(false),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
            'collapsed' => $this->boolean()->notNull()->defaultValue(false),
            'movable_u' => $this->boolean()->notNull()->defaultValue(true),
            'movable_d' => $this->boolean()->notNull()->defaultValue(true),
            'movable_l' => $this->boolean()->notNull()->defaultValue(true),
            'movable_r' => $this->boolean()->notNull()->defaultValue(true),
            'removable' => $this->boolean()->notNull()->defaultValue(true),
            'removable_all' => $this->boolean()->notNull()->defaultValue(false),
           'child_allowed' => $this->boolean()->notNull()->defaultValue(true),
		], $this->createTableOptions());

        $this->createIndex('tree_NK1', '{{%category}}', 'root');
        $this->createIndex('tree_NK2', '{{%category}}', 'lft');
        $this->createIndex('tree_NK3', '{{%category}}', 'rgt');
        $this->createIndex('tree_NK4', '{{%category}}', 'lvl');
        $this->createIndex('tree_NK5', '{{%category}}', 'active');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%category}}');
	}
}
