<?php

use yii\db\Migration;
use app\traits\migrations\CreateTableOptions;

/**
 * Handles the creation of table `{{%course}}`.
 */
class m220916_045838_create_course_table extends Migration
{
	use CreateTableOptions;

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%course}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255),
			'slug' => $this->string(255),
			'descr_1' => $this->text(),
			'descr_2' => $this->text(),
			'descr_3' => $this->text(),
			'descr_4' => $this->text(),
			'descr_5' => $this->text(),
			'descr_6' => $this->text(),
			'course_link' => $this->string(255),
			'school_id' => $this->integer(),
			'price' => $this->decimal(8, 2),
			'old_price' => $this->decimal(8, 2),
			'installment_rub' => $this->decimal(8, 2),
			'installment_month' => $this->integer(),
            'discount' => $this->decimal(8, 2),
            'promocode' => $this->string(255),
            'promocode_boxberry' => $this->string(255),
            'stock' => $this->text(),
            'start_date' => $this->date(),
            'duration' => $this->integer(),
            'lessons' => $this->text(),
            'format' => $this->text(),
            'features' => $this->text(),
            'categories' => $this->text(),
            'image' => $this->string(255),
            'filter_format' => $this->text(),
            'filter_features' => $this->text(),
            'priority' => $this->integer(),
            'status' => $this->integer(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'parser_id' => $this->integer(),
		], $this->createTableOptions());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%course}}');
	}
}
