<?php

use yii\db\Migration;
use app\traits\migrations\CreateTableOptions;

/**
 * Handles the creation of table `{{%free_course}}`.
 */
class m221002_213342_create_free_course_table extends Migration
{
	use CreateTableOptions;

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%free_course}}', [
			'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'course_link' => $this->string(255),
            'school_id' => $this->integer(),
            'promocode' => $this->string(255),
            'duration' => $this->integer(),
            'format' => $this->text(),
            'categories' => $this->text(),
            'status' => $this->integer()->defaultValue(1),
            'parser_id' => $this->integer(),
		], $this->createTableOptions());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%free_course}}');
	}
}
