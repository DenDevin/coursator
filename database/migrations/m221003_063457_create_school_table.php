<?php

use yii\db\Migration;
use app\traits\migrations\CreateTableOptions;

/**
 * Handles the creation of table `{{%school}}`.
 */
class m221003_063457_create_school_table extends Migration
{
	use CreateTableOptions;

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%school}}', [
			'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'slug' => $this->string(255),
            'school_link' => $this->string(255),
            'descr_1' => $this->text(),
            'descr_2' => $this->text(),
            'descr_3' => $this->text(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'image' => $this->string(255),
            'categories' => $this->text(),
            'benefits' => $this->text(),
            'limitations' => $this->text(),
            'stock' => $this->text(),
            'promocodes_link' => $this->text(),
            'courses_link' => $this->text(),
            'languages' => $this->text(),
            'hidden' => $this->tinyInteger()->defaultValue(0),
            'status' => $this->tinyInteger(),
		], $this->createTableOptions());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%school}}');
	}
}
