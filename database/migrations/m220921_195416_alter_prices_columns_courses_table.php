<?php

use yii\db\Migration;

/**
 * Class m220921_195416_alter_prices_columns_courses_table
 */
class m220921_195416_alter_prices_columns_courses_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
      $this->alterColumn('{{%course}}', 'price', $this->integer());
      $this->alterColumn('{{%course}}', 'old_price', $this->integer());
      $this->alterColumn('{{%course}}', 'installment_rub', $this->integer());
      $this->alterColumn('{{%course}}', 'discount', $this->integer());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
        $this->alterColumn('{{%course}}', 'price', $this->decimal(8, 2));
        $this->alterColumn('{{%course}}', 'old_price', $this->decimal(8, 2));
        $this->alterColumn('{{%course}}', 'installment_rub', $this->decimal(8, 2));
        $this->alterColumn('{{%course}}', 'discount', $this->decimal(8, 2));
	}


}
