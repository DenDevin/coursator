<?php

use yii\db\Migration;

/**
 * Class m221002_170234_alter_hidden_column_categories_table
 */
class m221002_170234_alter_hidden_column_categories_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
       $this->alterColumn('{{%category}}', 'hidden', $this->tinyInteger()->defaultValue(0));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
        $this->alterColumn('{{%category}}', 'hidden', $this->tinyInteger()->defaultValue(null));
	}

}
