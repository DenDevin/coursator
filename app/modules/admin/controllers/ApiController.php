<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\web\Controller;

class ApiController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


}