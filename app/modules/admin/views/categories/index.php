<?php

use yii\helpers\Html;
use app\modules\admin\widgets\TreeView\TreeView;
use app\models\Category\Category;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\models\enums\Common\YesNoStatus;
use yii\helpers\ArrayHelper;
use \yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Category\Category */
/* @var $searchModel app\models\Category\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="category-index">
    <div class="row">
        <div class="col-md-12">
            <?php Pjax::begin(['id' => 'tree-categories']); ?>
            <?= TreeView::widget([
                'query' => Category::find()->addOrderBy('root, lft'),
                'headingOptions' => ['label' => 'Категории'],
                'nodeView' => '@admin/widgets/TreeView/views/_form',
                'fontAwesome' => false,     // optional
                'isAdmin' => false,         // optional (toggle to enable admin mode)
                'displayValue' => 1,        // initial display value
                'softDelete' => true,       // defaults to true
                'cacheSettings' => [
                    'enableCache' => true   // defaults to true
                ]
            ]);?>
            <?php Pjax::end(); ?>
        </div>
        <div class="col-md-12">
            <?php Pjax::begin(['id' => 'grid-categories']); ?>
            <?= GridView::widget([
                'dataProvider'=> $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'filterInputOptions' => ['class' => 'form-control', 'style' => 'width:80px'],
                    ],
                    [
                        'attribute' => 'name',
                    ],
                    [
                        'attribute' => 'slug',
                        'filterInputOptions' => ['class' => 'form-control'],
                    ],
                    [
                        'attribute' => 'parent_id',
                        'value' => function ($model) {
                            if ($model->isRoot()) {
                                return 'Корневая категория';
                            } else {
                                return $model->parents(1)->one()->name;
                            }
                        },
                        'filter' => Select2::widget(
                            [
                                'model' => $searchModel,
                                'attribute' => 'parent_id',
                                'data' => ArrayHelper::map($searchModel::find()->addOrderBy('root, lft')->all(), 'id', 'name'),
                                'options' => ['placeholder' => '-'],
                                'language' => 'ru',
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]
                        ),
                    ],

                    [
                        'attribute' => 'order_id',
                        'filterInputOptions' => ['class' => 'form-control', 'style' => 'width:80px'],
                    ],

                    [
                        'attribute' => 'hidden',
                        'format' => 'raw',
                        'value' => function ($model) {
                           return (bool) $model->hidden ?
                               Html::tag('span', 'Скрыто', ['class' => 'badge badge-secondary']) :
                               Html::tag('span', 'Видимо', ['class' => 'badge badge-success']);
                        },


                        'filter' => Select2::widget(
                            [
                                'model' => $searchModel,
                                'attribute' => 'hidden',
                                'data' => YesNoStatus::listData(),
                                'options' => ['placeholder' => '-'],
                                'language' => 'ru',
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]
                        ),
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' =>  '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a(Html::tag('i', '',
                                    ['class' => 'fa fa-edit']),
                                    Url::to(['/admin/categories/update', 'id' => $model->id]), [
                                        'class' => 'btn btn-warning btn-sm'
                                    ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a(Html::tag('i', '',
                                    ['class' => 'fa fa-trash']),
                                    Url::to(['/admin/categories/delete', 'id' => $model->id]), [
                                        'class' => 'btn btn-danger btn-sm',
                                        'data' => [
                                            'confirm' => 'Вы уверены что хотите удалить данную категорию? Вы потеряете все данные после этого действия.',
                                            'method' => 'post',
                                            'data-pjax' => '0',
                                        ]
                                    ]);
                            }
                        ],
                    ]
                ],
            ]);?>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
