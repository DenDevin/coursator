<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Editor;
use app\modules\admin\assets\AssetBundle;
use app\modules\admin\assets\FilepondAsset;
use kartik\editors\Codemirror;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Category\Category;
use app\modules\admin\widgets\TreeView\TreeView;

FilepondAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Category\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

      <div class="row">
          <div class="col-md-6">
              <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
              <?= $form->field($model, 'h1')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
              <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
          </div>

          <div class="col-md-6">
              <?= $form->field($model, 'order_id')->textInput() ?>
          </div>

          <div class="col-md-2">
              <?= $form->field($model, 'image')->hiddenInput([
                  'id' => 'mainImage',
                  'data' => [
                      'id' => $model->id ? $model->id : 0,
                      'name' => 'Category[mainImage]'
                   ]

              ]) ?>
              <?= Html::img(!empty($model->image) ? $model->image : '/images/no-image.png', [
                  'class' => 'img-responsive w-100',
                  'id' => 'mainImageView',
                  'alt' => 'Изображение категории',
              ]) ?>
          </div>
          <div class="col-md-10">
              <?= $form->field($model, 'mainImage')->fileInput([
                  'class' => 'image-filepond',
                  'accept' => 'image/png, image/jpeg, image/gif',
              ]) ?>
          </div>

          <div class="col-md-12">
              <?= $form->field($model, 'descr_1')->widget(Editor::class, [
                  'settings' => [
                      'lang' => 'ru',
                      'minHeight' => 200,
                      'plugins' => [
                          'clips',
                          'fullscreen',
                      ],
                      'clips' => [
                          ['Lorem ipsum...', 'Lorem...'],
                      ],
                  ],
              ]); ?>
          </div>
          <div class="col-md-12">
              <?= $form->field($model, 'descr_2')->widget(Editor::class, [
                  'settings' => [
                      'lang' => 'ru',
                      'minHeight' => 200,
                      'plugins' => [
                          'clips',
                          'fullscreen',
                      ],
                      'clips' => [
                          ['Lorem ipsum...', 'Lorem...'],
                      ],
                  ],
              ]); ?>
          </div>


          <div class="col-md-12">
              <?= $form->field($model, 'superjob')->widget(Codemirror::class, [
                  'useKrajeePresets' => true,
                  'preset' => Codemirror::PRESET_JS,
              ]) ?>
          </div>
          <div class="col-md-12">
              <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-12">
              <?= $form->field($model, 'description')->textarea([
                      'rows' => 8
              ]); ?>
          </div>
          <div class="col-md-4">
              <?= $form->field($model, 'show_questionnaire')->checkbox() ?>
          </div>
          <div class="col-md-4">
              <?= $form->field($model, 'blocks_order')->checkbox() ?>
          </div>
          <div class="col-md-4">
              <?= $form->field($model, 'hidden')->checkbox() ?>
          </div>
      </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJsFile('/js/modules/admin/categories/_form.js', ['depends' => AssetBundle::class]); ?>
