<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Course\search\SearchCourse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'slug') ?>

    <?= $form->field($model, 'descr_1') ?>

    <?= $form->field($model, 'descr_2') ?>

    <?php // echo $form->field($model, 'descr_3') ?>

    <?php // echo $form->field($model, 'descr_4') ?>

    <?php // echo $form->field($model, 'descr_5') ?>

    <?php // echo $form->field($model, 'descr_6') ?>

    <?php // echo $form->field($model, 'course_link') ?>

    <?php // echo $form->field($model, 'school_id') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'old_price') ?>

    <?php // echo $form->field($model, 'installment_rub') ?>

    <?php // echo $form->field($model, 'installment_month') ?>

    <?php // echo $form->field($model, 'discount') ?>

    <?php // echo $form->field($model, 'promocode') ?>

    <?php // echo $form->field($model, 'promocode_boxberry') ?>

    <?php // echo $form->field($model, 'stock') ?>

    <?php // echo $form->field($model, 'start_date') ?>

    <?php // echo $form->field($model, 'duration') ?>

    <?php // echo $form->field($model, 'lessons') ?>

    <?php // echo $form->field($model, 'format') ?>

    <?php // echo $form->field($model, 'features') ?>

    <?php // echo $form->field($model, 'categories') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'filter_format') ?>

    <?php // echo $form->field($model, 'filter_features') ?>

    <?php // echo $form->field($model, 'priority') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'parser_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
