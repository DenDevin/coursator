<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Editor;
use kartik\select2\Select2;
use app\models\Category\Category;
use app\modules\admin\assets\AssetBundle;
use app\modules\admin\assets\FilepondAsset;
use app\helpers\JsonHelper;
use kartik\date\DatePicker;
use Carbon\Carbon;
use app\models\enums\Course\PublishStatus;
use yii\helpers\ArrayHelper;

FilepondAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Course\Course */
/* @var $category app\models\Category\Category */
/* @var $form yii\widgets\ActiveForm */

$model->categories = !empty($model->categories) ? JsonHelper::decode($model->categories) : [];

?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, 'image')->hiddenInput([
                'id' => 'mainImage',
                'data' => [
                    'id' => $model->id ? $model->id : 0,
                    'name' => 'Course[mainImage]'
                ]
            ]) ?>
            <?= Html::img(!empty($model->image) ? $model->image : '/images/no-image.png', [
                'class' => 'img-responsive w-100',
                'id' => 'mainImageView',
                'alt' => 'Изображение курса',
            ]) ?>
        </div>
        <div class="col-md-10">
            <?= $form->field($model, 'mainImage')->fileInput([
                'class' => 'image-filepond',
                'accept' => 'image/png, image/jpeg, image/gif',
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'descr_1')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'descr_2')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'descr_3')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'descr_4')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'descr_5')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'descr_6')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'course_link')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'school_id')->widget(Select2::class, [
                    'data' => [
                            1 => 'Школа 1',
                            2 => 'Школа 2',
                            3 => 'Школа 3',
                            4 => 'Школа 4',
                            5 => 'Школа 5',
                    ],
                   'options' => [
                         'prompt'  => 'Выберите школу...'
                   ]
            ]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'old_price')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'installment_rub')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'installment_month')->textInput() ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'promocode')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'promocode_boxberry')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'stock')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?php $model->start_date = Carbon::parse($model->start_date)->format('d.m.Y') ?>
            <?= $form->field($model, 'start_date')->widget(DatePicker::class, [
                'options' => ['placeholder' => 'Выберите дату начала курса...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'duration')->textInput(['type' => 'number']) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'lessons')->textarea(['rows' => 8]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'format')->textarea(['rows' => 8]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'features')->textarea(['rows' => 8]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'filter_features')->textarea(['rows' => 8]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'categories')->widget(Select2::class, [
                'theme' => Select2::THEME_BOOTSTRAP,
                'data' => ArrayHelper::map(Category::find()->
                orderBy('root, lft')->
                all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите категории..',
                    'multiple' => true,
                ],
            ]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'priority')->textInput() ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'status')->widget(Select2::class, [
                'data' => PublishStatus::listData(),
                'options' => [
                    'prompt'  => 'Выберите статус...'
                ]
            ]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'parser_id')->textInput() ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea([
                'rows' => 8
            ]); ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJsFile('/js/modules/admin/courses/_form.js', ['depends' => AssetBundle::class]); ?>
