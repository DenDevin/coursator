<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Course\Course */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="course-view">

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'slug',
            'descr_1:ntext',
            'descr_2:ntext',
            'descr_3:ntext',
            'descr_4:ntext',
            'descr_5:ntext',
            'descr_6:ntext',
            'course_link',
            'school_id',
            'price',
            'old_price',
            'installment_rub',
            'installment_month',
            'discount',
            'promocode',
            'promocode_boxberry',
            'stock:ntext',
            'start_date',
            'duration',
            'lessons:ntext',
            'format:ntext',
            'features:ntext',
            'categories:ntext',
            'image',
            'filter_format:ntext',
            'filter_features:ntext',
            'priority',
            'status',
            'title',
            'description:ntext',
            'parser_id',
        ],
    ]) ?>

</div>
