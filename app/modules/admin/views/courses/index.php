<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use Carbon\Carbon;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Category\Category;
use app\models\enums\Course\PublishStatus;
use app\helpers\HtmlHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Course\search\SearchCourse */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'grid-categories']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'style' => 'width:50px',
                ],
            ],
            [
                'attribute' => 'name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'style' => 'width:100px',
                ],
                'contentOptions' => [
                    'style' => ['width' => '100px;']
                ],
            ],
            [
                'attribute' => 'slug',
                'contentOptions' => [
                    'style' => ['width' => '100px;']
                ],
            ],
            [
                'attribute' => 'course_link',
                'contentOptions' => [
                    'style' => ['width' => '100px;']
                ],
                'format' => 'raw',
                'value' => function ($model) {
                   return Html::a($model->course_link, $model->course_link, [
                           'target' => '_blank'
                   ]);
                }
            ],
            [
                'attribute' => 'school_id',
                'contentOptions' => [
                    'style' => ['width' => '100px;']
                ],
            ],
            [
                'attribute' => 'start_date',
                'format' => 'text',
                'filterInputOptions' => ['style' => 'width:60px'],
                'contentOptions' => [
                    'style' => ['width' => '150px;']
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'pluginOptions' => [
                       // 'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ]
                ]),
               'value' => function ($model) {
                    return $model->start_date ?
                        Carbon::createFromFormat('Y-m-d', $model->start_date)->format('d.m.Y')
                        : '';
                }
            ],

            [
                'attribute' => 'categories',
                'format' => 'html',
                'filterInputOptions' => ['class' => 'form-control', 'style' => 'width:50px'],
                'contentOptions' => [
                    'style' => ['width' => '150px;']
                ],
                'value' => function ($model) {
                    return HtmlHelper::getHtmlCategoriesList($model);
                },
                'filter' => Select2::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'categories',
                        'data' => ArrayHelper::map(Category::find()->
                        addOrderBy('root, lft')->
                        all(), 'id', 'name'),
                        'options' => ['placeholder' => '-'],
                        'language' => 'ru',
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]
                ),
            ],
            [
                'attribute' => 'priority',
                'filterInputOptions' => ['class' => 'form-control', 'style' => 'width:60px'],
                'contentOptions' => [
                    'style' => ['width' => '60px;']
                ],
            ],
            [
                'attribute' => 'status',
                'filter' => Select2::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => PublishStatus::listData(),
                        'options' => ['placeholder' => '-'],
                        'language' => 'ru',
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]
                ),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::tag('span',
                        PublishStatus::getLabel($model->status),
                        ['class' => 'badge badge-primary']);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' =>  '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(Html::tag('i', '',
                            ['class' => 'fa fa-edit']),
                            Url::to(['/admin/courses/update', 'id' => $model->id]), [
                                    'class' => 'btn btn-warning btn-sm'
                            ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a(Html::tag('i', '',
                            ['class' => 'fa fa-trash']),
                            Url::to(['/admin/courses/delete', 'id' => $model->id]), [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'confirm' => 'Вы уверены что хотите удалить данный курс? Вы потеряете все данные после этого действия.',
                                    'method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            ]);
                    }
                ],
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
