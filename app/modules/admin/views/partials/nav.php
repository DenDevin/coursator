<?php

use yii\helpers\Html;
use yii\bootstrap4\Nav;
use app\modules\admin\widgets\Menu;

?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4 sidebar-dark-info">
	<!-- Brand Logo -->
	<a href="<?= Yii::$app->homeUrl; ?>" class="brand-link">
		<span class="brand-image" style="opacity: .8; line-height: 1.8em"></span>
		<span class="brand-text font-weight-light"><?= Html::encode(Yii::$app->name); ?></span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<i class="fas fa-user-tie img-circle elevation-2"></i>
			</div>
			<div class="info flex-grow-1">
				<a href="#" class="d-block"><?= Html::encode(Yii::$app->user->identity->getShortName()) ?></a>
			</div>
			<div class="align-self-end">
				<?= Html::a(
					'<i class="fa fa-power-off"></i>',
					['/auth/logout'],
					[
						'title'       => 'Выйти',
						'data-method' => 'post',
						'class'       => 'd-block',
						'style'       => 'line-height: 2em;',
					]
				); ?>
			</div>
		</div>
		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<?= Menu::widget([
				'options' => [
					'class' => 'nav nav-pills nav-sidebar flex-column',
					'data-widget' => 'treeview',
					'role'           => 'menu',
					'data-accordion' => 'false',
				],
				'items'   => [
					[
						'label' => 'Главная',
						'icon' => 'fas fa-tachometer-alt',
						'url' => ['/admin/dashboard'],
						'active' => 'dashboard' === Yii::$app->controller->id,
					],
					[
						'label'  => 'Пользователи',
						'icon'   => 'fas fa-users',
						'url'    => ['/admin/users'],
						'active' => 'users' === Yii::$app->controller->id,
					],
					[
						'label'  => 'Роли и Разрешения',
						'icon'   => 'fas fa-lock',
						'url'    => ['/admin/rbac/permissions'],
						'active' => 'permissions' === Yii::$app->controller->id,
					],
                    [
                        'label'  => 'Документы',
                        'icon'   => 'fas fa-file',
                        'url'    => ['#'],
                        'items' => [
                            [
                                'label'  => 'Категории',
                                'icon'   => 'fas fa-list',
                                'url'    => ['/admin/categories'],
                                'active' => 'categories' === Yii::$app->controller->id,
                            ],
                            [
                                'label'  => 'Курсы',
                                'icon'   => 'fas fa-graduation-cap',
                                'url'    => ['/admin/courses'],
                                'active' => 'courses' === Yii::$app->controller->id,
                            ],
                            [
                                'label'  => 'Бесплатные курсы',
                                'icon'   => 'fas fa-dollar-sign',
                                'url'    => ['/admin/free-courses'],
                                'active' => 'free-courses' === Yii::$app->controller->id,
                            ],
                            [
                                'label'  => 'Школы',
                                'icon'   => 'fas fa-school',
                                'url'    => ['/admin/school'],
                                'active' => 'school' === Yii::$app->controller->id,
                            ],
                        ],
                    ],

					[
						'label' => 'Настройки',
						'icon'  => 'fas fa-cog',
						'url'   => '#',
						'items' => [
							[
								'label' => 'Приложение',
								'url' => ['/admin/settings/app'],
							],
						],
					],
					/*[
						'label' => 'Developer',
						'icon'  => 'fas fa-code',
						'url'   => '#',
						'items' => [
							['label' => 'Gii', 'url' => ['/gii'], 'attributes' => 'target="_blank"'],
							['label' => 'Debug', 'url' => ['/debug'], 'attributes' => 'target="_blank"'],
						],
					],*/
				],
			]) ?>
		</nav>
	</div>

</aside>
