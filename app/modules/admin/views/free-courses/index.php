<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\assets\AssetBundle;
use kartik\select2\Select2;
use app\models\Category\Category;
use yii\helpers\ArrayHelper;
use app\helpers\HtmlHelper;
use app\models\enums\Course\FreePublishStatus;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Course\search\FreeCourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бесплатные курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="free-course-index">
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'grid-categories']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'filterInputOptions' => [
                        'class' => 'form-control',
                        'style' => 'width:50px',
                    ],
                    'contentOptions' => [
                        'style' => ['width' => '50px;']
                    ],
                ],
                [
                    'attribute' => 'name',
                    'filterInputOptions' => [
                        'class' => 'form-control',
                        'style' => 'width:150px',
                    ],
                    'contentOptions' => [
                        'style' => ['width' => '150px;']
                    ],
                ],
                [
                    'attribute' => 'course_link',
                    'contentOptions' => [
                        'style' => ['width' => '150px;']
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->course_link,
                            $model->course_link, [
                            'target' => '_blank'
                        ]);
                    }
                ],
                [
                    'attribute' => 'school_id',
                    'contentOptions' => [
                        'style' => ['width' => '150px;']
                    ],
                ],

                [
                    'attribute' => 'categories',
                    'format' => 'html',
                    'filterInputOptions' => ['class' => 'form-control', 'style' => 'width:50px'],
                    'contentOptions' => [
                        'style' => ['width' => '150px;']
                    ],
                    'value' => function ($model) {
                        return HtmlHelper::getHtmlCategoriesList($model);
                    },
                    'filter' => Select2::widget(
                        [
                            'model' => $searchModel,
                            'attribute' => 'categories',
                            'data' => ArrayHelper::map(Category::find()->
                            addOrderBy('root, lft')->
                            all(), 'id', 'name'),
                            'options' => ['placeholder' => '-'],
                            'language' => 'ru',
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]
                    ),
                ],
                [
                    'attribute' => 'status',
                    'filter' => Select2::widget(
                        [
                            'model' => $searchModel,
                            'attribute' => 'status',
                            'data' => FreePublishStatus::listData(),
                            'options' => ['placeholder' => '-'],
                            'language' => 'ru',
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]
                    ),
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::tag('span',
                            FreePublishStatus::getLabel($model->status),
                            ['class' => 'badge badge-primary']);
                    }
                ],


                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' =>  '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a(Html::tag('i', '',
                                ['class' => 'fa fa-edit']),
                                Url::to(['/admin/free-courses/update', 'id' => $model->id]), [
                                    'class' => 'btn btn-warning btn-sm'
                                ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a(Html::tag('i', '',
                                ['class' => 'fa fa-trash']),
                                Url::to(['/admin/free-courses/delete', 'id' => $model->id]), [
                                    'class' => 'btn btn-danger btn-sm',
                                    'data' => [
                                        'confirm' => 'Вы уверены что хотите удалить данный курс? Вы потеряете все данные после этого действия.',
                                        'method' => 'post',
                                        'data-pjax' => '0',
                                    ]
                                ]);
                        }
                    ],
                ]
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php $this->registerJsFile('/js/modules/admin/free-courses/_form.js', ['depends' => AssetBundle::class]); ?>