<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Course\FreeCourse */

$this->title = 'Редактирование: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Бесплатные курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="free-course-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
