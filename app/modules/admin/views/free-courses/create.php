<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Course\FreeCourse */

$this->title = 'Создание бесплатного курса';
$this->params['breadcrumbs'][] = ['label' => 'Бесплатные курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="free-course-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
