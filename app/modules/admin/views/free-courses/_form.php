<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Category\Category;
use app\modules\admin\assets\FilepondAsset;
use yii\helpers\ArrayHelper;
use app\models\enums\Course\FreePublishStatus;
use yii\helpers\Json;

FilepondAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Course\FreeCourse */
/* @var $form yii\widgets\ActiveForm */

$model->categories = !empty($model->categories) ?
    Json::decode($model->categories) :
    [];

?>

<div class="free-course-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'course_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'school_id')->widget(Select2::class, [
                'data' => [
                    1 => 'Школа 1',
                    2 => 'Школа 2',
                    3 => 'Школа 3',
                    4 => 'Школа 4',
                    5 => 'Школа 5',
                ],
                'options' => [
                    'prompt'  => 'Выберите школу...'
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'promocode')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'duration')->textInput(['type' => 'number']) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'format')->textarea(['rows' => 8]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'categories')->widget(Select2::class, [
                'theme' => Select2::THEME_BOOTSTRAP,
                'data' => ArrayHelper::map(Category::find()->
                orderBy('root, lft')->
                all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите категории..',
                    'multiple' => true,
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->widget(Select2::class, [
                'data' => FreePublishStatus::listData(),
                'options' => [
                    'prompt'  => 'Выберите статус...'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'parser_id')->textInput() ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
