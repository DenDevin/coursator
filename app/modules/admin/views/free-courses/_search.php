<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Course\search\FreeCourseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="free-course-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'course_link') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'promocode') ?>

    <?php // echo $form->field($model, 'duration') ?>

    <?php // echo $form->field($model, 'format') ?>

    <?php // echo $form->field($model, 'categories') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'parser_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
