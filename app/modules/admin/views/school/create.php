<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\School\School */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Школы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
