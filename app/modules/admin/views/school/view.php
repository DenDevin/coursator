<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\School\School */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="school-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'slug',
            'school_link',
            'descr_1:ntext',
            'descr_2:ntext',
            'descr_3:ntext',
            'title',
            'description:ntext',
            'image',
            'categories:ntext',
            'benefits:ntext',
            'limitations:ntext',
            'stock:ntext',
            'promocodes_link:ntext',
            'courses_link:ntext',
            'languages:ntext',
            'hidden',
            'status',
        ],
    ]) ?>

</div>
