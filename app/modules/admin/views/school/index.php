<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helpers\HtmlHelper;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Category\Category;
use app\models\enums\Common\YesNoStatus;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\School\search\SchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Школы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-index">

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'style' => 'width:50px',
                ],
            ],
            [
                'attribute' => 'name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'style' => 'width:100px',
                ],
                'contentOptions' => [
                    'style' => ['width' => '100px;']
                ],
            ],
            [
                'attribute' => 'slug',
                'contentOptions' => [
                    'style' => ['width' => '100px;']
                ],
            ],
            [
                'attribute' => 'school_link',
                'contentOptions' => [
                    'style' => ['width' => '100px;']
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->school_link, $model->school_link, [
                        'target' => '_blank'
                    ]);
                }
            ],

            [
                'attribute' => 'image',
                'contentOptions' => [
                    'style' => ['width' => '150px;']
                ],
                'format' => 'raw',
                'filter' => false,
                'value' => function ($model) {
                    return Html::img(!empty($model->image) ? $model->image : '/images/no-image.png', [
                        'class' => 'w-100 img-thumbnail'
                    ]);
                }
            ],

            [
                'attribute' => 'categories',
                'format' => 'html',
                'filterInputOptions' => ['class' => 'form-control', 'style' => 'width:50px'],
                'contentOptions' => [
                    'style' => ['width' => '150px;']
                ],
                'value' => function ($model) {
                    return HtmlHelper::getHtmlCategoriesList($model);
                },
                'filter' => Select2::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'categories',
                        'data' => ArrayHelper::map(Category::find()->
                        addOrderBy('root, lft')->
                        all(), 'id', 'name'),
                        'options' => ['placeholder' => '-'],
                        'language' => 'ru',
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]
                ),
            ],

            [
                'attribute' => 'hidden',
                'format' => 'raw',
                'value' => function ($model) {
                    return (bool) $model->hidden ?
                        Html::tag('span', 'Скрыто', ['class' => 'badge badge-secondary']) :
                        Html::tag('span', 'Видимо', ['class' => 'badge badge-success']);
                },


                'filter' => Select2::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'hidden',
                        'data' => YesNoStatus::listData(),
                        'options' => ['placeholder' => '-'],
                        'language' => 'ru',
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]
                ),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' =>  '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(Html::tag('i', '',
                            ['class' => 'fa fa-edit']),
                            Url::to(['/admin/school/update', 'id' => $model->id]), [
                                'class' => 'btn btn-warning btn-sm'
                            ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a(Html::tag('i', '',
                            ['class' => 'fa fa-trash']),
                            Url::to(['/admin/school/delete', 'id' => $model->id]), [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'confirm' => 'Вы уверены что хотите удалить данную школу? Вы потеряете все данные после этого действия.',
                                    'method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            ]);
                    }
                ],
            ]
        ],
    ]); ?>
</div>
