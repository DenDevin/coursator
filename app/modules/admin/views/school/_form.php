<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\assets\FilepondAsset;
use app\modules\admin\assets\AssetBundle;
use kartik\select2\Select2;
use app\models\Category\Category;
use yii\helpers\ArrayHelper;
use app\models\enums\School\Languages;
use vova07\imperavi\Widget as Editor;
use app\helpers\JsonHelper;

FilepondAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\School\School */
/* @var $form yii\widgets\ActiveForm */

$model->categories = !empty($model->categories) ? JsonHelper::decode($model->categories) : [];
$model->languages = !empty($model->languages) ? JsonHelper::decode($model->languages) : [];

?>

<div class="school-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'school_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'descr_1')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'descr_2')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'descr_3')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'image')->hiddenInput([
                'id' => 'mainImage',
                'data' => [
                    'id' => $model->id ? $model->id : 0,
                    'name' => 'School[mainImage]'
                ]
            ]) ?>
            <?= Html::img(!empty($model->image) ? $model->image : '/images/no-image.png', [
                'class' => 'img-responsive w-100',
                'id' => 'mainImageView',
                'alt' => 'Лого школы',
            ]) ?>
        </div>
        <div class="col-md-10">
            <?= $form->field($model, 'mainImage')->fileInput([
                'class' => 'image-filepond',
                'accept' => 'image/png, image/jpeg, image/gif',
            ]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'categories')->widget(Select2::class, [
                'theme' => Select2::THEME_BOOTSTRAP,
                'data' => ArrayHelper::map(Category::find()->
                orderBy('root, lft')->
                all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите категории..',
                    'multiple' => true,
                ],
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'benefits')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'limitations')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'stock')->widget(Editor::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                    ],
                    'clips' => [
                        ['Lorem ipsum...', 'Lorem...'],
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'promocodes_link')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'courses_link')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'languages')->widget(Select2::class, [
                'theme' => Select2::THEME_BOOTSTRAP,
                'data' => Languages::listData(),
                'options' => [
                    'placeholder' => 'Выберите языки..',
                    'multiple' => true,
                ],
            ]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'hidden')->checkbox() ?>
        </div>


    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJsFile('/js/modules/admin/school/_form.js', ['depends' => AssetBundle::class]); ?>
