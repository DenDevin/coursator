<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\School\School */

$this->title = 'Редактировать: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Школы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="school-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
