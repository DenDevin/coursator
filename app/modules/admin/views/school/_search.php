<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\School\search\SchoolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="school-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'slug') ?>

    <?= $form->field($model, 'school_link') ?>

    <?= $form->field($model, 'descr_1') ?>

    <?php // echo $form->field($model, 'descr_2') ?>

    <?php // echo $form->field($model, 'descr_3') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'categories') ?>

    <?php // echo $form->field($model, 'benefits') ?>

    <?php // echo $form->field($model, 'limitations') ?>

    <?php // echo $form->field($model, 'stock') ?>

    <?php // echo $form->field($model, 'promocodes_link') ?>

    <?php // echo $form->field($model, 'courses_link') ?>

    <?php // echo $form->field($model, 'languages') ?>

    <?php // echo $form->field($model, 'hidden') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
