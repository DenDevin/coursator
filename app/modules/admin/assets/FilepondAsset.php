<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;
use app\modules\admin\assets\AssetBundle as AdminAssetBundle;

class FilepondAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';

    public $css = [
        'css/filepond.css',
        'https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css',
    ];

    public $js = [
        'https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js',
        'https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.js',
        'https://unpkg.com/filepond-plugin-image-validate-size/dist/filepond-plugin-image-validate-size.js',
        'https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js',
        'https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js',
        'js/filepond.js',
        'js/functions.js',
    ];

    public $depends = [
        AdminAssetBundle::class,
    ];
}
