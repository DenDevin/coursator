FilePond.registerPlugin(
    FilePondPluginImagePreview,
);

const pond = FilePond.create(document.querySelector('input[class="image-filepond"]'), {
    labelIdle: `Перетащите вашу картинку или <span class="filepond--label-action">Загрузите</span>`,
    imagePreviewHeight: 200,
    imageResizeTargetWidth: 200,
    imageResizeTargetHeight: 200,
});