<?php

namespace app\modules\admin\assets;

use app\assets\FontAwesomeAsset;

class AssetBundle extends \yii\web\AssetBundle
{
	public $sourcePath = '@app/modules/admin/assets';

	public $css = [
		'css/admin.css',
		//'css/filepond.css',
	];

	public $js = [
      //  'js/filepond.js',
      //  'js/functions.js',
    ];

	public $depends = [
		AdminLteAsset::class,
		FontAwesomeAsset::class,
	];
}
