<?php

namespace app\models\Course;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "free_course".
 *
 * @property int $id
 * @property string $name
 * @property string $course_link
 * @property int $school_id
 * @property string $promocode
 * @property int $duration
 * @property string $format
 * @property string $categories
 * @property int $status
 * @property int $parser_id
 */
class FreeCourse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'free_course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['school_id', 'duration', 'status', 'parser_id'], 'integer'],
            [['name', 'course_link', 'school_id', 'categories'], 'required'],
            [['format'], 'string'],
            [['categories'], 'safe'],
            [['course_link'], 'url'],
            [['name', 'promocode'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'course_link' => 'Ссылка',
            'school_id' => 'Школа',
            'promocode' => 'Промокод',
            'duration' => 'Длительность',
            'format' => 'Формат',
            'categories' => 'Категории',
            'status' => 'Статус',
            'parser_id' => 'ID Парсера',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $this->normalizeCourseCategories();
        return parent::beforeSave($insert);
    }

    public function normalizeCourseCategories()
    {
        if ($this->categories && is_array($this->categories)) {
            $this->categories = Json::encode($this->categories);
        }
    }
}
