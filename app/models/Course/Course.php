<?php

namespace app\models\Course;

use app\behaviors\SluggableBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use Carbon\Carbon;
use app\helpers\JsonHelper;
use app\helpers\FileHelper;
use yii\helpers\Html;
use app\models\Category\Category;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $descr_1
 * @property string $descr_2
 * @property string $descr_3
 * @property string $descr_4
 * @property string $descr_5
 * @property string $descr_6
 * @property string $course_link
 * @property int $school_id
 * @property string $price
 * @property string $old_price
 * @property string $installment_rub
 * @property int $installment_month
 * @property string $discount
 * @property string $promocode
 * @property string $promocode_boxberry
 * @property string $stock
 * @property string $start_date
 * @property int $duration
 * @property string $lessons
 * @property string $format
 * @property string $features
 * @property string $categories
 * @property string $image
 * @property string $filter_format
 * @property string $filter_features
 * @property int $priority
 * @property int $status
 * @property string $title
 * @property string $description
 * @property int $parser_id
 */
class Course extends \yii\db\ActiveRecord
{

    public $mainImage;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(), [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $this->normalizeCourseCategories();
        $this->normalizeCourseStartDate();
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descr_1', 'descr_2', 'descr_3', 'descr_4', 'descr_5', 'descr_6', 'stock', 'lessons', 'format', 'features', 'filter_format', 'filter_features', 'description'], 'string'],
            [['school_id', 'installment_month', 'duration', 'priority', 'status', 'parser_id'], 'integer'],
            [['price', 'old_price', 'installment_rub', 'discount'], 'integer'],
            [['categories', 'mainImage'], 'safe'],
            [['start_date'], 'date', 'format' => 'dd.mm.yyyy'],
            [['name', 'course_link', 'school_id', 'price', 'categories'], 'required'],
            [['course_link'], 'url'],
            [['name', 'slug', 'course_link', 'promocode', 'promocode_boxberry', 'image', 'title'], 'string', 'max' => 255],
            [['slug'],
                'match',
                'pattern' => '/^[a-z0-9-]+$/',
                'message' => 'Ярлык (ЧПУ) может состоять только из латинских символов без пробелов'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Ярлык (ЧПУ)',
            'descr_1' => 'Описание 1',
            'descr_2' => 'Описание 2',
            'descr_3' => 'Описание 3',
            'descr_4' => 'Описание 4',
            'descr_5' => 'Описание 5',
            'descr_6' => 'Описание 6',
            'course_link' => 'Ссылка',
            'school_id' => 'Школа',
            'price' => 'Цена',
            'old_price' => 'Старая цена',
            'installment_rub' => 'Рассрочка (Руб.)',
            'installment_month' => 'Рассрочка (Мес.)',
            'discount' => 'Скидка',
            'promocode' => 'Промокод',
            'promocode_boxberry' => 'Промокод (Boxberry)',
            'stock' => 'Акция',
            'start_date' => 'Начало',
            'duration' => 'Длительность',
            'lessons' => 'Занятия',
            'format' => 'Формат',
            'features' => 'Особенности',
            'categories' => 'Категории',
            'image' => 'Изображение',
            'mainImage' => 'Загрузка изображения',
            'filter_format' => 'Формат для фильтров',
            'filter_features' => 'Особенности для фильтров',
            'priority' => 'Приоритет',
            'status' => 'Статус',
            'title' => 'Title',
            'description' => 'Description',
            'parser_id' => 'Парсер ID',
        ];
    }

    public function normalizeCourseCategories()
    {
        if ($this->categories && is_array($this->categories)) {
            $this->categories = JsonHelper::encode($this->categories);
        }
    }

    public function normalizeCourseStartDate()
    {
        if ($this->start_date) {
            if ($startDate = Carbon::createFromFormat('d.m.Y', $this->start_date)) {
                $this->start_date = $startDate->toDateString();
            }
        }
    }

    public function saveImage($courseId)
    {
        $this->mainImage = UploadedFile::getInstanceByName('Course[mainImage]');
        $fileName = Yii::$app->security->generateRandomString(16);
        if ($courseId !== 0) {
            $model = self::findOne($courseId);
            if (!empty($model->image) && file_exists(Yii::getAlias('@public') . $model->image)) {
                @unlink(Yii::getAlias('@public') . $model->image);
            }
        }
        $this->mainImage->saveAs('@images/courses/' . $fileName . '.' . $this->mainImage->extension);
        return '/images/courses/' . $fileName . '.' . $this->mainImage->extension;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            FileHelper::unlinkImage($this->image);
            return true;
        } else {
            return false;
        }
    }
}
