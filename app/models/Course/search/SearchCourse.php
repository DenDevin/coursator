<?php

namespace app\models\Course\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Course\Course;
use Carbon\Carbon;

/**
 * SearchCourse represents the model behind the search form of `app\models\Course\Course`.
 */
class SearchCourse extends Course
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'installment_month', 'duration', 'priority', 'status', 'parser_id'], 'integer'],
            [['name', 'slug', 'descr_1', 'descr_2', 'descr_3', 'descr_4', 'descr_5', 'descr_6', 'course_link', 'promocode', 'promocode_boxberry', 'stock', 'lessons', 'format', 'features', 'categories', 'image', 'filter_format', 'filter_features', 'title', 'description'], 'safe'],
            [['price', 'old_price', 'installment_rub', 'discount'], 'number'],
            [['start_date'], 'date', 'format' => 'dd.mm.yyyy'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->start_date) {
            $normalizedDate = Carbon::createFromFormat('d.m.Y', $this->start_date)->toDateString();
            $query->andFilterWhere([
                'IN', 'start_date', $normalizedDate
            ]);
        }

        if ($this->categories) {
            $query->andFilterWhere([
                'LIKE', 'categories', '%"' . $this->categories . '"%', false
            ]);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'school_id' => $this->school_id,
            'price' => $this->price,
            'old_price' => $this->old_price,
            'installment_rub' => $this->installment_rub,
            'installment_month' => $this->installment_month,
            'discount' => $this->discount,
           // 'start_date' => $this->start_date,
            'duration' => $this->duration,
            'priority' => $this->priority,
            'status' => $this->status,
            'parser_id' => $this->parser_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'descr_1', $this->descr_1])
            ->andFilterWhere(['like', 'descr_2', $this->descr_2])
            ->andFilterWhere(['like', 'descr_3', $this->descr_3])
            ->andFilterWhere(['like', 'descr_4', $this->descr_4])
            ->andFilterWhere(['like', 'descr_5', $this->descr_5])
            ->andFilterWhere(['like', 'descr_6', $this->descr_6])
            ->andFilterWhere(['like', 'course_link', $this->course_link])
            ->andFilterWhere(['like', 'promocode', $this->promocode])
            ->andFilterWhere(['like', 'promocode_boxberry', $this->promocode_boxberry])
            ->andFilterWhere(['like', 'stock', $this->stock])
            ->andFilterWhere(['like', 'lessons', $this->lessons])
            ->andFilterWhere(['like', 'format', $this->format])
            ->andFilterWhere(['like', 'features', $this->features])
        //    ->andFilterWhere(['like', 'categories', $this->categories])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'filter_format', $this->filter_format])
            ->andFilterWhere(['like', 'filter_features', $this->filter_features])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
