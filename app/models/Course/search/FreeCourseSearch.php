<?php

namespace app\models\Course\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Course\FreeCourse;

/**
 * FreeCourseSearch represents the model behind the search form of `app\models\Course\FreeCourse`.
 */
class FreeCourseSearch extends FreeCourse
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'duration', 'status', 'parser_id'], 'integer'],
            [['name', 'course_link', 'promocode', 'format', 'categories'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FreeCourse::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'school_id' => $this->school_id,
            'duration' => $this->duration,
            'status' => $this->status,
            'parser_id' => $this->parser_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'course_link', $this->course_link])
            ->andFilterWhere(['like', 'promocode', $this->promocode])
            ->andFilterWhere(['like', 'format', $this->format])
            ->andFilterWhere(['like', 'categories', $this->categories]);

        return $dataProvider;
    }
}
