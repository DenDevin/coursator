<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use Egulias\EmailValidator\Validation\NoRFCWarningsValidation;
use app\models\Email\Validator;


/**
 * Email model
 *

 */
class Email extends ActiveRecord
{

    public function checkEmail($request)
    {
        $response = [];
        $validator = new EmailValidator();
        $emailValidator = new Validator();

        $multipleValidations = new MultipleValidationWithAnd([
            new RFCValidation(),
            new DNSCheckValidation(),
            new NoRFCWarningsValidation(),
        ]);
        $response['email'] = $request['email'];
        $response['email_valid'] = $validator->isValid($request['email'], $multipleValidations);
        $emailExist = $emailValidator->validate($request['email']);
        $response['email_exist'] = (bool) $emailExist[$request['email']];
        return $response;
    }

}
