<?php

namespace app\models\enums\School;

use yii2mod\enum\helpers\BaseEnum;

class Languages extends BaseEnum
{
    const LANG_RU = 1;
    const LANG_EN = 2;
    const LANG_UA = 3;

    /**
     * @var string message category
     * You can set your own message category for translate the values in the $list property
     * Values in the $list property will be automatically translated in the function `listData()`
     */
    public static $messageCategory = 'app';

    /**
     * @var array
     */
    public static $list = [
        self::LANG_RU => 'Русский',
        self::LANG_EN => 'Английский',
        self::LANG_UA => 'Украинский',
    ];
}