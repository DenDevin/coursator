<?php

namespace app\models\enums\Course;

use yii2mod\enum\helpers\BaseEnum;

class FreePublishStatus extends BaseEnum
{
    const STATUS_PUBLISHED = 1;
    const STATUS_UNPUBLISHED = 2;

    /**
     * @var string message category
     * You can set your own message category for translate the values in the $list property
     * Values in the $list property will be automatically translated in the function `listData()`
     */
    public static $messageCategory = 'app';

    /**
     * @var array
     */
    public static $list = [
        self::STATUS_PUBLISHED => 'Опубликован',
        self::STATUS_UNPUBLISHED => 'Не опубликован',
    ];
}