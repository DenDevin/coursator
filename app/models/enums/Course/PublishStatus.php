<?php

namespace app\models\enums\Course;

use yii2mod\enum\helpers\BaseEnum;

class PublishStatus extends BaseEnum
{
    const STATUS_PUBLISHED = 1;
    const STATUS_UNPUBLISHED = 2;
    const STATUS_MARKED_ON_DELETION = 3;

    /**
     * @var string message category
     * You can set your own message category for translate the values in the $list property
     * Values in the $list property will be automatically translated in the function `listData()`
     */
    public static $messageCategory = 'app';

    /**
     * @var array
     */
    public static $list = [
        self::STATUS_PUBLISHED => 'Опубликован',
        self::STATUS_UNPUBLISHED => 'Не опубликован',
        self::STATUS_MARKED_ON_DELETION => 'Отмечен на удаление',
    ];
}