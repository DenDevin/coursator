<?php

namespace app\models\Category\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Category\Category;
use yii\helpers\ArrayHelper;

/**
 * CategorySearch represents the model behind the search form of `app\models\Category\Category`.
 */
class CategorySearch extends Category
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'order_id', 'show_questionnaire', 'root', 'lft', 'rgt', 'lvl', 'icon_type', 'active', 'selected', 'disabled', 'readonly', 'visible', 'collapsed', 'movable_u', 'movable_d', 'movable_l', 'movable_r', 'removable', 'removable_all', 'hidden'], 'integer'],
            [['name', 'h1', 'slug', 'descr_1', 'descr_2', 'image', 'superjob', 'title', 'description', 'icon'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->parent_id) {
            $parent = self::findOne($this->parent_id);
            $childs = ArrayHelper::map($parent->children(1)->all(), 'id', 'id');
            $query->andFilterWhere([
                'IN', 'id', $childs
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'show_questionnaire' => $this->show_questionnaire,
            'hidden' => $this->hidden,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'h1', $this->h1])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'descr_1', $this->descr_1])
            ->andFilterWhere(['like', 'descr_2', $this->descr_2])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'superjob', $this->superjob])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
