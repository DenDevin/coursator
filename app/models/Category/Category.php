<?php

namespace app\models\Category;

use kartik\tree\TreeSecurity;
use Yii;
use kartik\tree\models\TreeTrait;
use kartik\tree\models\Tree;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\helpers\FileHelper;
use yii\web\Response;
use yii\web\UploadedFile;
use app\behaviors\SluggableBehavior;
use creocoder\nestedsets\NestedSetsBehavior;


/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property string $h1
 * @property string $slug
 * @property int $parent_id
 * @property string $descr_1
 * @property string $descr_2
 * @property string $image
 * @property int $order_id
 * @property string $superjob
 * @property int $show_questionnaire
 * @property string $title
 * @property string $description
 * @property int $root
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property string $icon
 * @property int $icon_type
 * @property int $active
 * @property int $selected
 * @property int $disabled
 * @property int $readonly
 * @property int $visible
 * @property int $collapsed
 * @property int $movable_u
 * @property int $movable_d
 * @property int $movable_l
 * @property int $movable_r
 * @property int $removable
 * @property int $removable_all
 */
class Category extends ActiveRecord
{
    use TreeTrait;
    public $mainImage;
    public $activeOrig = true;
    public $nodeRemovalErrors = [];

    public static $boolKeys = [
        'isAdmin',
        'softDelete',
        'showFormButtons',
        'showIDAttribute',
        'showNameAttribute',
        'multiple',
        'treeNodeModify',
        'allowNewRoots',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
   public function behaviors()
    {
       return ArrayHelper::merge(
           parent::behaviors(), [
                 'sluggable' => [
                     'class' => SluggableBehavior::class,
                     'attribute' => 'name',
                     'ensureUnique' => true,
                 ],
                 'nestedSets' => [
                     'class' => NestedSetsBehavior::class,
                     'depthAttribute' => 'lvl',
                     'treeAttribute' => 'root',
                 ]
       ]);
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'h1'], 'required'],
            [['parent_id', 'order_id', 'show_questionnaire', 'icon_type', 'active', 'selected', 'disabled', 'readonly', 'visible', 'collapsed', 'movable_u', 'movable_d', 'movable_l', 'movable_r', 'removable', 'removable_all', 'child_allowed', 'blocks_order', 'hidden'], 'integer'],
            [['descr_1', 'descr_2', 'superjob', 'description'], 'string'],
            [['mainImage'], 'safe'],
            [['name', 'h1', 'slug', 'image', 'title', 'icon'], 'string', 'max' => 255],
            [['slug'],
                'match',
                'pattern' => '/^[a-z0-9-]+$/',
                'message' => 'Ярлык (ЧПУ) может состоять только из латинских символов без пробелов'
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'h1' => 'Заглавие H1',
            'slug' => 'Ярлык (ЧПУ)',
            'parent_id' => 'Родительская категория',
            'descr_1' => 'Описание 1',
            'descr_2' => 'Описание 2',
            'image' => 'Изображение',
            'mainImage' => 'Загрузка изображения',
            'order_id' => 'Порядок',
            'superjob' => 'Superjob',
            'show_questionnaire' => 'Показать опросник',
            'title' => 'Title',
            'description' => 'Description',
            'root' => 'Корень',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'lvl' => 'Lvl',
            'icon' => 'Иконка',
            'icon_type' => 'Тип иконки',
            'active' => 'Активный',
            'selected' => 'Выбран',
            'disabled' => 'Неактивен',
            'readonly' => 'Только чтение',
            'visible' => 'Видимый',
            'collapsed' => 'Свернутый',
            'blocks_order' => 'Другой порядок блоков',
            'hidden' => 'Скрыто',
            'movable_u' => 'Movable U',
            'movable_d' => 'Movable D',
            'movable_l' => 'Movable L',
            'movable_r' => 'Movable R',
            'removable' => 'Removable',
            'removable_all' => 'Removable All',
        ];
    }

    public function saveImage($catId)
    {
        $this->mainImage = UploadedFile::getInstanceByName('Category[mainImage]');
        $fileName = Yii::$app->security->generateRandomString(16);
        if ($catId !== 0) {
            $model = self::findOne($catId);
            if (!empty($model->image) && file_exists(Yii::getAlias('@public') . $model->image)) {
                @unlink(Yii::getAlias('@public') . $model->image);
            }
        }
        $this->mainImage->saveAs('@images/categories/' . $fileName . '.' . $this->mainImage->extension);
        return '/images/categories/' . $fileName . '.' . $this->mainImage->extension;
    }

    public function getParentCategory()
    {
        if ($parent = $this->parents(1)->one()) {
            return $parent->id;
        }
        return null;
    }


    public function updateCategoryParent()
    {

            if (!$this->isRoot() && $this->getIsNewRecord()) {
                $nodeFrom = self::findOne($this->id);
                if ($nodeFrom->isMovable('r')) {
                    $nodeTo = self::findOne($this->parent_id);
                    $nodeFrom->appendTo($nodeTo);
                    if ($nodeFrom->save(false)) {
                        return true;
                    }
                }
            }

    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            FileHelper::unlinkImage($this->image);
            return true;
        } else {
            return false;
        }
    }










}
