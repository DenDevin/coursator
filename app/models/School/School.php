<?php

namespace app\models\School;

use Yii;
use app\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use Carbon\Carbon;
use app\helpers\JsonHelper;
use app\helpers\FileHelper;
use yii\helpers\Html;
use app\models\Course\Course;

/**
 * This is the model class for table "school".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $school_link
 * @property string $descr_1
 * @property string $descr_2
 * @property string $descr_3
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $categories
 * @property string $benefits
 * @property string $limitations
 * @property string $stock
 * @property string $promocodes_link
 * @property string $courses_link
 * @property string $languages
 * @property int $hidden
 * @property int $status
 */
class School extends \yii\db\ActiveRecord
{
    public $mainImage;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(), [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $this->normalizeSchoolCategories();
        $this->normalizeSchoolLanguages();
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'categories', 'school_link', 'image', 'promocodes_link', 'courses_link', 'languages'], 'required'],
            [['descr_1', 'descr_2', 'descr_3', 'description', 'benefits', 'limitations', 'stock', 'promocodes_link', 'courses_link'], 'string'],
            [['hidden', 'status'], 'integer'],
            [['name', 'slug', 'school_link', 'title', 'image'], 'string', 'max' => 255],
            [['categories', 'languages', 'mainImage'], 'safe'],
            [['school_link', 'promocodes_link', 'courses_link'], 'url'],
            [['slug'],
                'match',
                'pattern' => '/^[a-z0-9-]+$/',
                'message' => 'Ярлык (ЧПУ) может состоять только из латинских символов без пробелов'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Ярлык (ЧПУ)',
            'school_link' => 'Ссылка',
            'descr_1' => 'Описание 1',
            'descr_2' => 'Описание 2',
            'descr_3' => 'Описание 3',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Логотип',
            'mainImage' => 'Загрузка изображения',
            'categories' => 'Категории',
            'benefits' => 'Плюсы',
            'limitations' => 'Минусы',
            'stock' => 'Акция',
            'promocodes_link' => 'Ссылка на промокоды',
            'courses_link' => 'Ссылка на курсы',
            'languages' => 'Языки',
            'hidden' => 'Скрыто',
            'status' => 'Статус',
        ];
    }

    public function saveImage($schoolId)
    {
        $this->mainImage = UploadedFile::getInstanceByName('School[mainImage]');
        $fileName = Yii::$app->security->generateRandomString(16);
        if ($schoolId !== 0) {
            $model = self::findOne($schoolId);
            if (!empty($model->image) && file_exists(Yii::getAlias('@public') . $model->image)) {
                @unlink(Yii::getAlias('@public') . $model->image);
            }
        }
        $this->mainImage->saveAs('@images/schools/' . $fileName . '.' . $this->mainImage->extension);
        return '/images/schools/' . $fileName . '.' . $this->mainImage->extension;
    }

    public function normalizeSchoolCategories()
    {
        if ($this->categories && is_array($this->categories)) {
            $this->categories = JsonHelper::encode($this->categories);
        }
    }

    public function normalizeSchoolLanguages()
    {
        if ($this->languages && is_array($this->languages)) {
            $this->languages = JsonHelper::encode($this->languages);
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            FileHelper::unlinkImage($this->image);
            if ($this->courses) {
                foreach ($this->courses as $course) {
                    $course->delete();
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function getCourses()
    {
        return $this->hasMany(Course::class, [
            'school_id' => 'id'
        ]);
    }
}
