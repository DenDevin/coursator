<?php

namespace app\models\School\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\School\School;

/**
 * SchoolSearch represents the model behind the search form of `app\models\School\School`.
 */
class SchoolSearch extends School
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'hidden', 'status'], 'integer'],
            [['name', 'slug', 'school_link', 'descr_1', 'descr_2', 'descr_3', 'title', 'description', 'image', 'categories', 'benefits', 'limitations', 'stock', 'promocodes_link', 'courses_link', 'languages'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = School::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->categories) {
            $query->andFilterWhere([
                'LIKE', 'categories', '%"' . $this->categories . '"%', false
            ]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hidden' => $this->hidden,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'school_link', $this->school_link])
            ->andFilterWhere(['like', 'descr_1', $this->descr_1])
            ->andFilterWhere(['like', 'descr_2', $this->descr_2])
            ->andFilterWhere(['like', 'descr_3', $this->descr_3])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'benefits', $this->benefits])
            ->andFilterWhere(['like', 'limitations', $this->limitations])
            ->andFilterWhere(['like', 'stock', $this->stock])
            ->andFilterWhere(['like', 'promocodes_link', $this->promocodes_link])
            ->andFilterWhere(['like', 'courses_link', $this->courses_link])
            ->andFilterWhere(['like', 'languages', $this->languages]);

        return $dataProvider;
    }
}
