<?php

namespace app\assets;

use yii\bootstrap4\BootstrapAsset;
use yii\web\YiiAsset;

class VueAssetBundle extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/src';

    public $css = [

    ];
    public $js = [
        'js/app.js'
    ];
    public $depends = [

    ];
}
