
window.Vue = require('vue');
require('./plugins/vue-particles');
require('./plugins/vue-axios');
require('./plugins/vuelidate');
require('./plugins/languages');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


import ExistEmail from './components/ExistEmail.vue';
import SomeComponent from './components/SomeComponent.vue';


const app = new Vue({
    el: '#app',
    components: {
        ExistEmail,
        SomeComponent
    }
});
