<?php

namespace app\behaviors;

use yii\db\BaseActiveRecord;
use yii\helpers\Inflector;
use Yii;
use yii\validators\UniqueValidator;
use Cocur\Slugify\Slugify;
use Cocur\Slugify\RuleProvider\DefaultRuleProvider;

/**
 * Class SluggableBehavior
 * @package app\behaviors
 *
 */
class SluggableBehavior extends \yii\behaviors\SluggableBehavior
{

    /**
     * Transliterator
     * @var string
     */
    public $transliterator;
    public $imploder = null;
    /**
     * Update slug attribute even it already exists
     * @var bool
     */
    public $forceUpdate = true;

    /**
     * @inheritdoc
     */
    protected function getValue($event)
    {
        $isNewSlug = true;
        $slug = null;
        if ($this->attribute !== null && !Yii::$app->request->isAjax) {
            $attributes = (array)$this->attribute;
            /* @var $owner BaseActiveRecord */
            $owner = $this->owner;
            if (!$owner->getIsNewRecord() && !empty($owner->{$this->slugAttribute})) {
                $isNewSlug = false;
                foreach ($attributes as $attribute) {
                    if ($owner->isAttributeChanged($attribute) && $this->forceUpdate) {
                        $isNewSlug = true;
                        break;
                    }
                }
            }

            if ($isNewSlug) {
                    $slugModel = new Slugify((array) [], (new DefaultRuleProvider()));
                    $slug = $slugModel->slugify($owner->{$this->attribute});
                if ($this->ensureUnique) {

                    $baseSlug = $slug;
                    if ($founded = $owner->find()->where([
                        'slug' => $baseSlug
                    ])->one()) {
                        if ($last = $owner->find()->orderBy('id DESC')->one()) {
                            if (!is_null($this->imploder)) {
                                $slug = $slug . '-' . $this->imploder;
                                $slug = $slugModel->slugify($slug);
                            } else {
                                $slug = $slug . '-' . $last->id;
                                $slug = $slugModel->slugify($slug);
                            }
                            return $slug;
                        }
                    }
                }

            } else {
                $slug = $owner->{$this->slugAttribute};
            }
        }

        return $slug;
    }

    /**
     * Checks if given slug value is unique.
     * @param string $slug slug value
     * @return boolean whether slug is unique.
     */
    protected  function validateSlug($slug)
    {
        /* @var $validator UniqueValidator */
        /* @var $model BaseActiveRecord */
        $validator = Yii::createObject(array_merge(
            [
                'class' => UniqueValidator::class
            ],
            $this->uniqueValidator
        ));

        $model = clone $this->owner;
        $model->clearErrors();
        $model->{$this->slugAttribute} = $slug;

        $validator->validateAttribute($model, $this->slugAttribute);
        return !$model->hasErrors();
    }

    /**
     * Generates slug using configured callback or increment of iteration.
     * @param string $baseSlug base slug value
     * @param integer $iteration iteration number
     * @return string new slug value
     * @throws \yii\base\InvalidConfigException
     */
    protected function generateUniqueSlug($baseSlug, $iteration)
    {
        if (is_callable($this->uniqueSlugGenerator)) {
            return call_user_func($this->uniqueSlugGenerator, $baseSlug, $iteration, $this->owner);
        } else {
            return $baseSlug . '-' . ($iteration + 1);
        }
    }

}