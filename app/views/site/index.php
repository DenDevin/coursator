<?php

/* @var $this yii\web\View */

use app\assets\AssetBundle;
use yii\helpers\Json;

$assets = AssetBundle::register($this);

?>
<div class="site-index">
    <h1 class="display-4">Coursator</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad alias animi deserunt dolorem ex nesciunt nisi porro ratione saepe tempore.</p>
</div>