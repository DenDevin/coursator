<?php

namespace app\helpers;

use yii\helpers\Html;
use yii\helpers\Html as BaseHtmlHelper;
use app\models\Category\Category;
use app\helpers\JsonHelper;

class HtmlHelper extends BaseHtmlHelper
{
    public static function getHtmlCategoriesList($model)
    {
        $html = "";
        if (!empty($model->categories) /*&&
            JsonHelper::isJson($model->categories)*/) {
            $categories = JsonHelper::decode($model->categories, true);
            if (is_array($categories)) {
                $html .= Html::beginTag('ul', ['class' => 'list-group']);
                foreach ($categories as $catId) {
                    $category = Category::find()->select('name')->where(['id' => $catId])->one();
                    $html .= Html::tag('li', $category->name, ['class' => 'badge badge-secondary mb-1']);
                }
                $html .= Html::endTag('ul');
            }
        }
        return $html;
    }
}