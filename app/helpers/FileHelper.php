<?php

namespace app\helpers;

use Yii;
use yii\helpers\Html;
use yii\helpers\FileHelper as BaseFileHelper;


class FileHelper extends BaseFileHelper
{
    public static function unlinkImage($path)
    {
        if (!empty($path) && file_exists(Yii::getAlias('@public') . $path)) {
            @unlink(Yii::getAlias('@public') . $path);
            return true;
        } else {
            return false;
        }
    }
}