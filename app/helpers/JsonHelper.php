<?php

namespace app\helpers;

use yii\helpers\Json as BaseJsonHelper;

class JsonHelper extends BaseJsonHelper
{
    public static function isJson($string)
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}