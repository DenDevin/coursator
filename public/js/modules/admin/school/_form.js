pond.setOptions({
    server: {
        process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
            const formData = new FormData();
            let param = $('meta[name=csrf-param]').attr('content');
            let token = $('meta[name=csrf-token]').attr('content');
            let schoolId = $('input[data-name="' + fieldName + '"]').data('id');
            formData.append(param, token);
            formData.append('schoolId', schoolId);
            formData.append(fieldName, file, file.name);
            console.log('Form submitted: ', formData);
            const request = new XMLHttpRequest();
            request.open('POST', '/admin/school/upload-image');
            request.upload.onprogress = (e) => {
                progress(e.lengthComputable, e.loaded, e.total);
            };
            request.onload = function () {
                if (request.status >= 200 && request.status < 300) {
                    let resp = request.responseText;
                    load(resp);
                    $('#mainImageView').attr('src', resp);
                    $('#mainImage').val(resp);

                    // return resp;
                } else {
                    error('oh no');
                }
            };

            request.send(formData);
            return {
                abort: () => {
                    request.abort();
                    abort();
                },
            };
        },
    },
});
